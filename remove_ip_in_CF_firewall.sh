# Remove IPs blocked in Cloudflare.
#
# total - number of IPS blocked in CF
# myid - The ID of the IP blocked

# The script will loop and remove the IP blocked in CF firewall
# created a script to remove thousands of IPs blocked via Fail2ban filter in CF firewall

CFKey=<your CF API Key>
CFEmail=<your email>
CFAccountID=<CF account ID>

total=$(curl -s -X GET "https://api.cloudflare.com/client/v4/accounts/$CFAccountID/firewall/access_rules/rules?mode=block&configuration_target=ip&page=1&per_page=1&match=all" \
     -H "X-Auth-Email: $CFEmail" \
     -H "X-Auth-Key: $CFKey" \
     -H "Content-Type: application/json" | jq -r '.result_info["total_count"]')

for (( c=1; c<=$total; c++ ))
do
   sleep 1
   
   myid=$(curl -s -X GET "https://api.cloudflare.com/client/v4/accounts/$CFAccountID/firewall/access_rules/rules?mode=block&configuration_target=ip&page=1&per_page=1&match=all" \
     -H "X-Auth-Email: $CFEmail" \
     -H "X-Auth-Key: $CFKey" \
     -H "Content-Type: application/json" | jq -r '.result[] | .id')

   curl -X DELETE "https://api.cloudflare.com/client/v4/user/firewall/access_rules/rules/$myid" \
     -H "X-Auth-Email: $CFEmail" \
     -H "X-Auth-Key: $CFKey" \
     -H "Content-Type: application/json"

done
